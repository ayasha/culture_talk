-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 11. Dez 2015 um 03:56
-- Server-Version: 10.1.8-MariaDB
-- PHP-Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `culture_talk`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `headline` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pictures` varchar(100) DEFAULT NULL,
  `topic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `con_friends`
--

CREATE TABLE `con_friends` (
  `con_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

--
-- Daten für Tabelle `con_friends`
--

INSERT INTO `con_friends` (`con_id`, `user_id`, `friend_id`) VALUES
(14, 1, 2),
(15, 2, 1),
(16, 1, 14),
(17, 14, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `head` tinytext NOT NULL,
  `text` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `author` varchar(20) CHARACTER SET utf8 NOT NULL,
  `headline` varchar(100) CHARACTER SET utf8 NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pictures` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `topic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

--
-- Daten für Tabelle `news`
--

INSERT INTO `news` (`id`, `author`, `headline`, `text`, `timestamp`, `pictures`, `topic_id`) VALUES
(5, 'test_user', 'Kafka', 'Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas BÃ¶ses getan hÃ¤tte, wurde er eines Morgens verhaftet. Â»Wie ein Hund!Â« sagte er, es war, als sollte die Scham ihn Ã¼berleben. Als Gregor Samsa eines Morgens aus unruhigen TrÃ¤umen erwachte, fand er sich in seinem Bett zu einem ungeheueren Ungeziefer verwandelt. \r\n<br><br>\r\nUnd es war ihnen wie eine BestÃ¤tigung ihrer neuen TrÃ¤ume und guten Absichten, als am Ziele ihrer Fahrt die Tochter als erste sich erhob und ihren jungen KÃ¶rper dehnte. Â»Es ist ein eigentÃ¼mlicher ApparatÂ«, sagte der Offizier zu dem Forschungsreisenden und Ã¼berblickte mit einem gewissermaÃŸen bewundernden Blick den ihm doch wohlbekannten Apparat. Sie hÃ¤tten noch ins Boot springen kÃ¶nnen, aber der Reisende hob ein schweres, geknotetes Tau vom Boden, drohte ihnen damit und hielt sie dadurch von dem Sprunge ab. In den letzten Jahrzehnten ist das Interesse an HungerkÃ¼nstlern sehr zurÃ¼ckgegangen. Aber sie Ã¼berwanden sich, umdrÃ¤ngten den KÃ¤fig und wollten sich gar nicht fortrÃ¼hren.Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas BÃ¶ses getan hÃ¤tte, wurde er eines Morgens verhaftet. Â»Wie ein Hund!Â« sagte er, es war, als sollte die Scham ihn Ã¼berleben. Als Gregor Samsa eines Morgens aus unruhigen TrÃ¤umen erwachte, fand er sich', '2015-12-06 15:19:52', NULL, 3),
(6, 'test_user', 'CDU-Wirtschaftsrat attackiert Merkels Flüchtlingspolitik ', 'Der Wirtschaftsflügel der CDU hat scharfe Kritik an der Flüchtlingspolitik von Bundeskanzlerin Angela Merkel (CDU) geÃ¤uÃŸert und ihr einen â€žSonderwegâ€œ in Europa vorgeworfen. Wenige Tage vor dem Parteitag in Karlsruhe forderte der Wirtschaftsrat der CDU: â€žDas Entscheidende ist, die hohen Zahlen der Zuwanderer deutlich zu reduzieren. DafÃ¼r muss die Bundesregierung endlich klare Botschaften an alle richten, die auf gepackten Koffern sitzenâ€œ, sagte Wolfgang Steiger, der GeneralsekretÃ¤r des Wirtschaftsrats der CDU, der F.A.Z..<br><br>\r\nDurch â€žverschiedene falsche Botschaftenâ€œ sei eine Sogwirkung auf FlÃ¼chtlinge nach Deutschland entstanden. Es sollten dringend Obergrenzen fÃ¼r FlÃ¼chtlinge national und europÃ¤ische festgelegt werden, fordert der Wirtschaftsrat. Deutschland habe sich in der EuropÃ¤ischen Union â€ždurch seinen Sonderweg zunehmen isoliertâ€œ, warnte Steiger. â€žDurch die einseitige Aussetzung des Dublin-Abkommens haben wir uns Ã¼ber europÃ¤isches Recht hinweg gesetzt.â€œ In diesem Jahr sind von der Bundespolizei schon etwa 950.000 Asylbewerber registriert worden. Viele weitere sind noch nicht registriert worden. Der Wirtschaftsrat spricht von SchÃ¤tzungen bis 1,5 Millionen Asyl-Zuwanderern in diesem Jahr. Wenn weiter so viele kÃ¤men, wÃ¤ren die Haushalte und Sozialsysteme Ã¼berfordert und die gesellschaftliche Akzeptanz wÃ¼rde schwinden, sagte Steiger.', '2015-12-10 10:32:08', NULL, 1),
(8, 'test_user', 'Gold und Smaragde könnten bis zu 17 Milliarden Dollar wert sein', 'Jahrelangen Rechtsstreit geführt\r\n\r\nSelbst im Falle einer erfolgreichen Bergung ist fraglich, ob der SchatzKolumbien alleine zusteht. Es gebe noch viele offene Fragen, sagte der Historiker und Experte für Seerecht, Daniel de Narváez, der Zeitung "El Tiempo": "Verklagen Spanien und Peru den kolumbianischen Staat? Verlangt die spanische Regierung das Schiff als Staatsbesitz zurück?"\r\n\r\nMit der US-Firma Sea Search Armada hat Kolumbien bereits einen langen Rechtsstreit ausgefochten. Bereits in den 80er Jahren behauptete das Unternehmen, das Wrack geortet zu haben und wollte am Gewinn beteiligt werden. Er wurden eine ganze Reihe von Urteilen gefällt, im Oktober 2011 wies ein US-Gericht schließlich alle Ansprüche von Sea Search Armada als unbegründet zurück.\r\n\r\nDer alte Streit könnte nun neu aufflammen. "Jetzt müssen die Kolumbianer natürlich mit uns verhandeln", sagte der Anwalt der Firma, Danilo Devis, in "El Tiempo". "Denn wir haben ihnen schon 1982 die Koordinaten den Fundorts gegeben."\r\n\r\nGoldmünzen und Edelsteine aus den Kolonien\r\n\r\nDie "San José» war am 8. Juni 1708 vor der Insel Rosario an der Karibikküste gesunken, nachdem sie von einem englischen Flottenverband angegriffen worden war. Das Schiff sollte Goldmünzen und Edelsteine aus den amerikanischen Kolonien nach Spanien bringen.\r\n\r\nAn der Ortung des Wracks war auch ein Experte beteiligt, der gemeinsam mit Kollegen 1985 das Wrack der "Titanic" entdeckt hatte. Bei der Suche nahe der Insel Rosario setzten die Wissenschaftler Sonar, Spezialkameras und Unterwasserdrohen ein.', '2015-12-06 22:43:27', NULL, 1),
(10, 'test_user', 'Feuerwehrmann stirbt bei Löscharbeiten in Schleswig-Holstein', 'Bei Löscharbeiten in einem Eisenwarenladen ist in Marne in Schleswig-Holstein ein Feuerwehrmann ums Leben gekommen. Der Mann habe sich bei dem Einsatz am Sonntagmorgen in dem brennenden Haus aufgehalten, sagte ein Polizeisprecher.<br><br>\r\nNach Angaben der Feuerwehr in Schleswig-Holstein war es der erste tödliche Unfall in deren Reihen seit neun Jahren.\r\n\r\nDer junge Mann war demnach mit Atemschutzgeräten im Haus. Wie es dort zu dem Unglück kam, ist noch unklar. Ein weiterer Feuerwehrmann wurde leicht verletzt in ein 150 Feuerwehrleute im Einsatz\r\n\r\nDie Kriminalpolizei hat Ermittlungen zu dem Unglück aufgenommen. Die Ausrüstung des Feuerwehrmanns wurde beschlagnahmt. Seine Kameraden wurden seelsorgerisch betreut.\r\n\r\nRund 150 Feuerwehrmänner waren im Einsatz, um den Brand in dem Wohn- und Geschäftsgebäude in der Marner Innenstadt unter Kontrolle zu bringen. Noch nach Stunden löschte die Feuerwehr auflodernde Glutnester.\r\n\r\nDas Gebäude brannte vollständig aus. Die Brandursache ist noch nicht bekannt. Der Eisenwarenladen sollte zum Jahresende schließen. Die durch Marne führende Bundesstraße B5 war in beide Richtungen voll gesperrt.\r\n\r\n\r\n<p>üüüüüüüüüüüääääääääääöööööööö</p>', '2015-12-06 22:46:51', NULL, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pinnwand`
--

CREATE TABLE `pinnwand` (
  `pinnwand_id` int(11) NOT NULL,
  `post_ids` varchar(100) DEFAULT NULL,
  `blog_ids` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `pinnwand`
--

INSERT INTO `pinnwand` (`pinnwand_id`, `post_ids`, `blog_ids`) VALUES
(1, NULL, NULL),
(2, NULL, NULL),
(13, NULL, NULL),
(14, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `posts`
--

INSERT INTO `posts` (`post_id`, `user_id`, `text`, `timestamp`) VALUES
(1, 1, 'ich wünschte: ich hätte eine pause', '2015-12-10 16:45:58'),
(2, 2, 'ein kleines lila häuschen xD', '2015-12-10 16:46:25'),
(3, 14, 'so langsam habe ich hunger', '2015-12-10 16:46:55'),
(4, 1, 'bla bla bla bla', '2015-12-10 16:47:21'),
(5, 13, 'ich bin user 13', '2015-12-10 16:47:38');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `profile`
--

CREATE TABLE `profile` (
  `profil_id` int(11) NOT NULL,
  `books` varchar(100) DEFAULT NULL,
  `films` varchar(100) DEFAULT NULL,
  `music` varchar(100) DEFAULT NULL,
  `avatar` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `topics`
--

CREATE TABLE `topics` (
  `topic_id` int(11) NOT NULL,
  `topic` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `topics`
--

INSERT INTO `topics` (`topic_id`, `topic`) VALUES
(1, 'Politik'),
(2, 'Kunst'),
(3, 'Kultur'),
(4, 'Wissenschaft');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 NOT NULL,
  `pw` varchar(30) COLLATE utf8_german2_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_german2_ci NOT NULL,
  `rights` int(11) NOT NULL DEFAULT '1',
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `books` varchar(300) COLLATE utf8_german2_ci DEFAULT NULL,
  `films` varchar(300) COLLATE utf8_german2_ci DEFAULT NULL,
  `music` varchar(300) COLLATE utf8_german2_ci DEFAULT NULL,
  `avatar` varchar(300) COLLATE utf8_german2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`user_id`, `username`, `pw`, `email`, `rights`, `online`, `books`, `films`, `music`, `avatar`) VALUES
(1, 'test_user', '123', 'test@test.de', 1, 0, NULL, NULL, NULL, NULL),
(2, 'dark_dreams', '345', 'dark_dreams@web.de', 1, 0, NULL, NULL, NULL, NULL),
(13, 'genau ;)', '123', 'neu@neu.de', 1, 0, NULL, NULL, NULL, NULL),
(14, 'haha', '123', 'haha@haha.de', 1, 0, NULL, NULL, NULL, NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `author` (`author`);

--
-- Indizes für die Tabelle `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indizes für die Tabelle `con_friends`
--
ALTER TABLE `con_friends`
  ADD PRIMARY KEY (`con_id`);

--
-- Indizes für die Tabelle `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indizes für die Tabelle `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `pinnwand`
--
ALTER TABLE `pinnwand`
  ADD PRIMARY KEY (`pinnwand_id`);

--
-- Indizes für die Tabelle `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indizes für die Tabelle `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`profil_id`),
  ADD UNIQUE KEY `profil_id` (`profil_id`),
  ADD KEY `profil_id_2` (`profil_id`),
  ADD KEY `profil_id_3` (`profil_id`);

--
-- Indizes für die Tabelle `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `con_friends`
--
ALTER TABLE `con_friends`
  MODIFY `con_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT für Tabelle `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `pinnwand`
--
ALTER TABLE `pinnwand`
  MODIFY `pinnwand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT für Tabelle `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `profile`
--
ALTER TABLE `profile`
  MODIFY `profil_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `topics`
--
ALTER TABLE `topics`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`author`) REFERENCES `profile` (`profil_id`);

--
-- Constraints der Tabelle `pinnwand`
--
ALTER TABLE `pinnwand`
  ADD CONSTRAINT `pinnwand_ibfk_1` FOREIGN KEY (`pinnwand_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
