<?php

//@author Diana

namespace app\model;

class c_profile_model
{
    private $connect;
    private $username = 'student';
    private $password = 'pwa';
    private $host = 'localhost';
    
    static private $instance = null;
    
    //--------------------------------------------------------------------------
    //API
    //--------------------------------------------------------------------------
    
    static public function m_get_single_instance()
    {
        if(null === self::$instance)
        {
            self::$instance = new self;
        }
        return self::instance;
    }
    
    //--------------------------------------------------------------------------
    
    public function m_get_login_data($_userid)
    {
        $this->m_p_get_login_data($_userid);
    }
    
    public function m_get_friends($_userid)
    {
        $this->m_p_get_friends($_userid);
    }
    
    public function m_get_profile($_userid)
    {
        $this->m_p_get_profile($_userid);
    }
    
    
    //--------------------------------------------------------------------------
    //private
    //--------------------------------------------------------------------------

    private function m_p_get_login_data($_username)
    {
        $query = 'SELECT username, user_id, pw FROM user WHERE username = '.$_username;
        
        $this->m_p_connect();
        
        $result = mysqli($this->connect, $query);
        $data = mysqli_fetch_array($result);
        
        $login = array('user_id' => $data['user_id'], 'username' => $data['username'],
                        'pw' => $data['pw']);
        
        $this->m_p_close();
        
        return $login;
    }
    
    private function m_p_get_friends($_userid)
    {
        $friends = array();
        $query = 'SELECT b.user_id, online, username FROM con_friends a JOIN user b ON a.friend_id = b.user_id WHERE a.user_id = '.$_userid;
        
        $this->m_p_connect();
        
        $result = mysqli($this->connect, $query);
        
        for($i = 0; $data = mysqli_fetch_array($result); $i++)
        {
            $friends[$i] = array('user_id' => $data['user_id'], 'username' => $data[username], 
                                 'online' => $data['online']);
        }
        
        $this->m_p_close();
        
        
        
        return $friends;
    }
    
    private function m_p_get_profile($_userid)
    {
        $query = 'SELECT * FROM user WHERE user_id = '.$_userid;
        
        $this->m_p_connect();
        
        $result = mysqli($this->connect, $query);
        $data = mysqli_fetch_array($result);
        
        $profile = array('username' => $data['username'], 'email' => $data['email'],
                         'books' => $data['books'], 'films' => $data['films'],
                         'music' => $data['music'], 'avatar' => $data['avatar']);
        
        $this->m_p_close();
        
        return $profile;
    }
    
    private function m_p_connect()
    {
        $this->connect = mysqli_connect($this->host, $this->username, $this->password)
                or die('DB connection failed: '.mysqli_error());
    }
    
    private function m_p_close()
    {
        mysqli_close($this->connect) or die("Closing DB failed: ".mysqli_error());
    }
    
    private function __construct()
    {
        
    }
    
    private function __clone()
    {
        
    }
}
?>