<?php

    namespace app\control;
    
    require_once 'app/core/renderer.php';
    require_once 'app/control/test.controller.php';
    require_once 'app/control/c_profile.controller.php';

    
    use app\core\CRenderer;
    use app\control\CNewsController;
    use app\control\testController;
    use app\control\profileController;

    /*
     * This should be a wrapper for our main program flow.
     * Useful to have an overview.
     * 
     * Certainly this should be extended for your use. (for example different application states)
     */
    
    class CApplication
    {
        private $m_title       = '';
        private $m_subtitle    = '';
        
        private $m_navigation  = array(
                                0 => array('page' => 'team', 'title' => 'TEAM'),
                                1 => array('page' => 'work', 'title' => 'WORK'),
                                2 => array('page' => 'service', 'title' => 'SERVICE'),
                                3 => array('page' => 'test', 'title' => 'ich bin im Navi')
        );
        
        private $m_renderer    = null;
        private $m_GET         = null;
        private $m_POST        = null;
        
        //title and description are only given for testing to the constructor (look at index.php to understand the progress)
        public function __construct($_applicationTitle, $_applicationSubtitle)
        {
            $this->m_title       = $_applicationTitle;
            $this->m_subtitle    = $_applicationSubtitle;
            $this->m_renderer    = CRenderer::getInstance(); // get an instance from the singleton renderer
            
            //feel free to use them ;) these are the global PHP requests
            $this->m_GET         = $_GET;  // variables provided to the script via URL query string
            $this->m_POST        = $_POST; // variables provided to the script via HTTP POST
        }
        
        public function run()
        {
            $this->renderHeader();
            $this->renderContent();
            $this->renderFooter();
        }
        
        private function getNavigationString()
        {
            $navigation = '';

            for($i = 0; $i < count($this->m_navigation); ++$i)
            {
                $navigation .= '<a href="?page='.$this->m_navigation[$i]['page'].'">'.$this->m_navigation[$i]['title'].'</a>';
            }
            
            return $navigation;
        }
        
        private function renderHeader()
        {
            $this->m_renderer->loadTemplate('header.html');
            $this->m_renderer->assign(array('title'       => isset($this->m_GET['page']) ? $this->m_title . ' - ' . strtoupper($this->m_GET['page']) : $this->m_title, 
                                            'assetPath'   => 'app/view/assets/css/',
                                            'imgPath'     => 'app/view/assets/img/',
                                            'headline'    => $this->m_title,
                                            'subheadline' => $this->m_subtitle,
                                            'navigation'  => $this->getNavigationString()));
            $this->m_renderer->render();
        }
        
        private function renderContent()
        {
            if(isset($this->m_GET['page']))
            {
                switch ($this->m_GET['page'])
                {
                    case 'news':
                        
                        $newsController = new CNewsController();
                        
                        if(isset($this->m_GET['article_id']))
                        {
                            $newsController->viewArticleById($this->m_GET['article_id']);
                        }
                        else
                        {
                            $newsController->viewAllArticles();
                        }
                        
                        break;
                        
                    case 'test':
                        $testController = new testController();
                        $testController->textausgabe();
                        break;

                    default:
                        
                        $this->m_renderer->loadTemplate('error.html');
                        $this->m_renderer->assign(array('errorcode' => 404, 'errormessage' => 'The requested page was not found.'));
                        $this->m_renderer->render();
                        
                        break;
                }
            }
            else
            {
            }
        }
        
        private function renderFooter()
        {
            $this->m_renderer->loadTemplate('footer.html');
            $this->m_renderer->render();
        }
    }
    
?>