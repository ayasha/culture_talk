<?php
namespace app\control;

require_once 'app\core\renderer.php';
require_once 'app\model\profile.model.php';

use app\core\CRenderer;
use app\model\c_profile_model;

/**
 * Description of c_profile_controller
 *
 * @author Diana
 */
class c_profile_controller {
    
    private $db;
    private $friends;
    private $email;
    private $books;
    private $music;
    private $films;
    
    public function __construct()
    {
        $this->db = c_profile_controller::m_get_single_instance();
    }
    
    
    public function m_get_profile()
    {
        
    }
    
    public function m_set_profile()
    {
        
    }
    
    public function m_login()
    {
        
    }
}
